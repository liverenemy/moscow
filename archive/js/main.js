(function($) {
    $.fn.reverse = [].reverse;

    function checkMenus() {
        var $window = $(window);
        var windowWidth = parseInt($window.outerWidth());


        var scrollTop = parseInt($window.scrollTop());
        var headerHeight = $('.page__block-header').outerHeight();
        var $nav = $('.nav');

        var hasCurrentMenuItem = false;
        var $oldCurrentNavItem = $('.nav ul li.current');

        $('.page__block').each(function() {
            var $self = $(this);
            if ($self.attr('id')) {
                var menuItemForId = $self.attr('id');
                var topY = parseInt($self.offset().top - scrollTop);
                var bottomY = parseInt(topY + $self.outerHeight());
                if (topY <= 0 && bottomY > 0) {
                    hasCurrentMenuItem = true;
                    var $newCurrentNavItem = $('.nav ul li[data-for-id="' + menuItemForId + '"]');
                    if (!$newCurrentNavItem.hasClass('current')) {
                        $oldCurrentNavItem.removeClass('current');
                        $newCurrentNavItem.addClass('current');
                        console.log(menuItemForId);
                    }
                }
            }
        });

        if (!hasCurrentMenuItem) {
            $oldCurrentNavItem.removeClass('current');
        }

        if (parseInt(scrollTop) > parseInt(headerHeight)) {
            $nav.addClass('fixed');
        } else {
            $nav.removeClass('fixed');
        }

        var $sharingBanner = $('.sharing');
        var sharingMinimumScrollTop = 1120;
        var isDesktopLayout = windowWidth > 576;

        if (windowWidth <= 1920) {
            sharingMinimumScrollTop = sharingMinimumScrollTop / 1920 * windowWidth;
        }

        if (isDesktopLayout && scrollTop > sharingMinimumScrollTop) {
            if (!$sharingBanner.hasClass('fixed')) {
                $sharingBanner.addClass('fixed');
            }
        } else {
            if ($sharingBanner.hasClass('fixed')) {
                $sharingBanner.removeClass('fixed');
            }
        }

        if (windowWidth <= 576) {
            $sharingBanner.css('right', 'auto');
        } else if ($sharingBanner.hasClass('fixed'))  {
            if (windowWidth <= 1920) {
                $sharingBanner.css('right', 0);
                $nav.css('right', 0);
            } else {
                var $mainpage = $('.page');
                var mainpageRight = windowWidth - parseInt($mainpage.offset().left) - parseInt($mainpage.outerWidth());
                $nav.css('right', mainpageRight);
                $sharingBanner.css('right', mainpageRight);
            }
        } else {
            $sharingBanner.css('right', 0);
        }

        var $menuButtonContainer = $('.aside').first();

        $('.mainpage__wrapper')
            .each(function() {
                var clientTop = parseInt($(this).offset().top) - parseInt(scrollTop);
                var clientBottom = clientTop + parseInt($(this).outerHeight());
                if (clientTop <= 0 && clientBottom >= 0) {
                    var addMenuClass = $(this).data('add-menu-class');
                    var removeMenuClass = $(this).data('remove-menu-class');
                    if (addMenuClass) {
                        $menuButtonContainer.addClass(addMenuClass);
                    }

                    if (removeMenuClass) {
                        $menuButtonContainer.removeClass(removeMenuClass);
                    }
                }
            })
        ;
    }

    $(document).ready(() => {
        checkMenus();

        $('.page__block-header-slider').slick({
            arrows: false,
            cssEase: 'linear',
            dots: true,
            fade: true,
            infinite: false,
            adaptiveHeight: false,
            autoPlay: false
        });
    });

    $(document).on('click', '.page-block__nav-trigger', function() {
        $('body').toggleClass('nav-close');
    });

    $(document).on('click', '.toggle-collapsable', function() {
        var $collapsable = $(this).closest('.collapsable');
        var $parent = $collapsable.parent();
        var wasExpanded = $collapsable.hasClass('expanded');
        $parent.find('.collapsable').each(function() {
            $(this).removeClass('expanded');
        });
        if (!wasExpanded) {
            $collapsable.addClass('expanded');
        }
    });

    $(document).on('click', '.page__block-content-item-content-item-hashtag', function() {
        var $self = $(this);
        var $container = $self.closest('.page__block-content-item-content-item');
        var $hashtagContainer = $self.closest('.page__block-content-item-content-item-hashtags');
        var cssClass = $self.data('css-class');
        var $hashtags = $hashtagContainer.find('.page__block-content-item-content-item-hashtag');
        $hashtags
            .each(function() {
                var $innerSelf = $(this);
                var innerCssClass = $innerSelf.data('css-class');
                if (innerCssClass !== cssClass) {
                    $innerSelf.removeClass('chosen');
                    $container.removeClass(innerCssClass);
                }
            })
        ;
        $self.toggleClass('chosen');
        $container.toggleClass(cssClass);
        var isAnyHashTagChosen = !!$hashtagContainer.find('.page__block-content-item-content-item-hashtag.chosen').length;
        if (isAnyHashTagChosen) {
            $container.removeClass('all');
        } else {
            $container.addClass('all');
        }
    });

    $(document).on('change', '[name="page__block-routes-filter-form-district"]', function() {
        var $input = $(this);
        var cssClass = $input.val();
        var districtName = $input.closest('.styled-select-item').find('.styled-select-item-label').text().trim();
        $input
            .closest('.page__block-content-item-content-item-header-container')
            .find('.page__block-content-item-content-item-header')
            .html(districtName)
        ;
        var cssClasses = $input
            .closest('.styled-select')
            .find('[name="page__block-routes-filter-form-district"]')
            .map(function() {
                return $(this).val();
            })
            .get()
        ;

        var $container = $input.closest('.page__block-content-item-content-item');
        cssClasses.forEach(function(currentCssClass) {
            $container.removeClass(currentCssClass);
        });
        $container.addClass(cssClass);
    });

    $(document).on('keyup', '[name="page__block-routes-filter-form-name"]', function() {
        var $input = $(this);
        var searchString = $input.val().toLowerCase();
        var $container = $input.closest('.page__block-content-item-content-item');
        var $allItems = $container.find('.collapsable');
        $allItems.each(function() {
            var $collapsable = $(this);
            var routeNameOriginal = $collapsable.data('name').trim();
            var routeName = routeNameOriginal.toLowerCase();
            var authorNameOriginal = $collapsable.data('author').trim();
            var authorName = authorNameOriginal.toLowerCase();

            var $routeNameContainer = $collapsable.find('.route-name').first();
            var $authorNameContainer = $collapsable.find('.route-author').first();

            var isAnythingFound = false;

            if (searchString.length && routeName.indexOf(searchString) !== -1) {
                $routeNameContainer.html(
                    routeName.replace(
                        searchString,
                        [
                            '<span class="highlight-found">',
                                searchString,
                            '</span>'
                        ].join('')
                    )
                );
                isAnythingFound = true;
            } else {
                $routeNameContainer.html(routeNameOriginal);
            }

            if (searchString.length && authorName.indexOf(searchString) !== -1) {
                $authorNameContainer.html(
                    authorName.replace(
                        searchString,
                        [
                            '<span class="highlight-found">',
                            searchString,
                            '</span>'
                        ].join('')
                    )
                );

                isAnythingFound = true;
            } else {
                $authorNameContainer.html(authorNameOriginal);
            }

            if (!isAnythingFound && searchString.length) {
                $collapsable.addClass('hidden');
            } else {
                $collapsable.removeClass('hidden');
            }
        });
    });

    $(window).on('scroll', checkMenus);

    $(window).on('resize', checkMenus);
})(jQuery);