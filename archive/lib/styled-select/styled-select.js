function clearForm(myFormElement) {
    var elements = myFormElement.elements;

    myFormElement.reset();

    for(i=0; i<elements.length; i++) {
        var element = elements[i];
        var field_type = element.type.toLowerCase();
        var isCleared = false;

        switch(field_type) {

            case "text":
            case "password":
            case "textarea":
            case "hidden":

                element.value = "";
                isCleared = true;
                break;

            case "radio":
            case "checkbox":
                if (element.checked) {
                    element.checked = false;
                    isCleared = true;
                }
                break;

            case "select-one":
            case "select-multi":
                element.selectedIndex = -1;
                isCleared = true;
                break;

            default:
                break;
        }

        var elementId = element.id;
        if (isCleared && elementId) {
            $('#' + elementId).trigger('clear');
        }
    }
}

(function($) {
    $(document).on('click', '.styled-select-title', function () {
        $(this).closest('.styled-select').toggleClass('collapsed');
    });

    $(document).on('click', '.styled-select-item-label', function () {
        var $input = $(this).prev();
        var $styledSelect = $input.closest('.styled-select');
        var $title = $styledSelect.find('.styled-select-title').first();
        var newTitleContent = $.trim($input.next().html());
        $styledSelect.find('.styled-select-item-input').each(function() {
            $(this).attr('checked', false);
        });
        $input.attr('checked', 'checked');
        if (!$input.is(':checked') || !newTitleContent) {
            newTitleContent = $styledSelect.data('placeholder');
        }
        if (!newTitleContent) {
            newTitleContent = 'Пожалуйста, выберите';
        }

        if ($title.length) {
            $title.html('<nobr>' + newTitleContent + '</nobr>');
        }
        $styledSelect.addClass('collapsed');
    });

    $(document).on('clear', '.styled-select-item-input', function() {
        var $styledSelect = $(this).closest('.styled-select');
        var $title = $styledSelect.find('.styled-select-title').first();
        var newTitleContent = $styledSelect.data('placeholder');
        if (!newTitleContent) {
            newTitleContent = 'Пожалуйста, выберите';
        }

        if ($title.length) {
            $title.html('<nobr>' + newTitleContent + '</nobr>');
        }
        $styledSelect.addClass('collapsed');
    });

    $(document).on('click', 'body', function(event) {
        var $dropdown = $('.styled-select:not(.collapsed)');
        var $target = $(event.target);
        if(!$target.closest('.styled-select').length) {
            $dropdown.addClass('collapsed');
        }
    });
})(jQuery);