(function($) {
    $.fn.reverse = [].reverse;

    function checkMenus() {
        var $window = $(window);
        var windowWidth = parseInt($window.outerWidth());


        var scrollTop = parseInt($window.scrollTop());
        var headerHeight = $('.page__block-header').outerHeight();
        var $nav = $('.nav');

        var hasCurrentMenuItem = false;
        var $oldCurrentNavItem = $('.nav ul li.current');

        $('.page__block').each(function() {
            var $self = $(this);
            if ($self.attr('id')) {
                var menuItemForId = $self.attr('id');
                var topY = parseInt($self.offset().top - scrollTop);
                var bottomY = parseInt(topY + $self.outerHeight());
                if (topY <= 0 && bottomY > 0) {
                    hasCurrentMenuItem = true;
                    var $newCurrentNavItem = $('.nav ul li[data-for-id="' + menuItemForId + '"]');
                    if (!$newCurrentNavItem.hasClass('current')) {
                        $oldCurrentNavItem.removeClass('current');
                        $newCurrentNavItem.addClass('current');
                    }
                }
            }
        });

        if (!hasCurrentMenuItem) {
            $oldCurrentNavItem.removeClass('current');
        }

        if (parseInt(scrollTop) > parseInt(headerHeight)) {
            $nav.addClass('fixed');
        } else {
            $nav.removeClass('fixed');
        }

        var $sharingBanner = $('.sharing');
        var sharingMinimumScrollTop = 619;
        var isDesktopLayout = windowWidth > 576;

        if (windowWidth <= 1920) {
            sharingMinimumScrollTop = sharingMinimumScrollTop / 1920 * windowWidth;
        }

        if (isDesktopLayout && scrollTop > sharingMinimumScrollTop) {
            if (!$sharingBanner.hasClass('fixed')) {
                $sharingBanner.addClass('fixed');
            }
        } else {
            if ($sharingBanner.hasClass('fixed')) {
                $sharingBanner.removeClass('fixed');
            }
        }

        if (windowWidth <= 576) {
            $sharingBanner.css('right', 'auto');
        } else if ($sharingBanner.hasClass('fixed'))  {
            if (windowWidth <= 1920) {
                $sharingBanner.css('right', 0);
                $nav.css('right', 0);
            } else {
                var $mainpage = $('.page');
                var mainpageRight = windowWidth - parseInt($mainpage.offset().left) - parseInt($mainpage.outerWidth());
                $nav.css('right', mainpageRight);
                $sharingBanner.css('right', mainpageRight);
            }
        } else {
            $sharingBanner.css('right', 0);
        }

        var $menuButtonContainer = $('.aside').first();

        $('.mainpage__wrapper')
            .each(function() {
                var clientTop = parseInt($(this).offset().top) - parseInt(scrollTop);
                var clientBottom = clientTop + parseInt($(this).outerHeight());
                if (clientTop <= 0 && clientBottom >= 0) {
                    var addMenuClass = $(this).data('add-menu-class');
                    var removeMenuClass = $(this).data('remove-menu-class');
                    if (addMenuClass) {
                        $menuButtonContainer.addClass(addMenuClass);
                    }

                    if (removeMenuClass) {
                        $menuButtonContainer.removeClass(removeMenuClass);
                    }
                }
            })
        ;
    }

    $(document).ready(() => {
        checkMenus();
    });

    $(document).on('click', '.page-block__nav-trigger', function() {
        $('body').toggleClass('nav-close');
    });

    $(document).on('click', '.toggle-esse', function() {
        $(this).closest('.page__block-esse-list-content-item').toggleClass('expanded');
    });

    $(window).on('scroll', checkMenus);

    $(window).on('resize', checkMenus);

    function onStoppedAudio() {
        var $parent = $(this).closest('.page__block-jury-content-item');
        $parent.removeClass('is-playing');
    }

    $(document).on('click', '.page__block-jury-content-item-info-audio-play', function() {
        var $parent = $(this).closest('.page__block-jury-content-item');
        var $audio = $parent.find('audio')[0];
        if ($parent.hasClass('is-playing')) {
            $audio.pause();
        } else {
            $audio.play();
            $($audio).on('pause', onStoppedAudio);
            $($audio).on('ended', onStoppedAudio);
            $parent.addClass('is-playing');
        }
    });

    $(document).on('click', '.page__block-esse-list-content-item-button-view', function() {
        var $esseParent = $(this).closest('.page__block-esse-list-content-item');
        var $esseContent = $esseParent.find('.page__block-esse-list-content-item-content');
        var $youtubeIframe = $esseContent.find('iframe');
        var youTubeHtml = $youtubeIframe.prop('outerHTML');
        console.log(youTubeHtml);
        var messageBox = window.SimpleLightbox.open({
            content: youTubeHtml,
            elementClass: 'slbContentEl'
        });
    });
})(jQuery);