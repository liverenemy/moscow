(function($) {
    $.fn.reverse = [].reverse;

    function getRandomInt(max) {
        return Math.round(Math.random() * max);
    }

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

    function checkMenus() {
        var $window = $(window);
        var windowWidth = parseInt($window.outerWidth());

        var scrollTop = parseInt($window.scrollTop());
        var headerHeight = $('.page__block-header').outerHeight();
        var $nav = $('.nav');

        var hasCurrentMenuItem = false;
        var $oldCurrentNavItem = $('.nav ul li.current');

        $('.page__block').each(function() {
            var $self = $(this);
            if ($self.attr('id')) {
                var menuItemForId = $self.attr('id');
                var topY = parseInt($self.offset().top - scrollTop);
                var bottomY = parseInt(topY + $self.outerHeight());
                if (topY <= 0 && bottomY > 0) {
                    hasCurrentMenuItem = true;
                    var $newCurrentNavItem = $('.nav ul li[data-for-id="' + menuItemForId + '"]');
                    if (!$newCurrentNavItem.hasClass('current')) {
                        $oldCurrentNavItem.removeClass('current');
                        $newCurrentNavItem.addClass('current');
                    }
                }
            }
        });

        if (!hasCurrentMenuItem) {
            $oldCurrentNavItem.removeClass('current');
        }

        if (parseInt(scrollTop) > parseInt(headerHeight)) {
            $nav.addClass('fixed');
        } else {
            $nav.removeClass('fixed');
        }

        var $sharingBanner = $('.sharing');
        var sharingMinimumScrollTop = 619;
        var isDesktopLayout = windowWidth > 576;

        if (windowWidth <= 1920) {
            sharingMinimumScrollTop = sharingMinimumScrollTop / 1920 * windowWidth;
        }

        if (isDesktopLayout && scrollTop > sharingMinimumScrollTop) {
            if (!$sharingBanner.hasClass('fixed')) {
                $sharingBanner.addClass('fixed');
            }
        } else {
            if ($sharingBanner.hasClass('fixed')) {
                $sharingBanner.removeClass('fixed');
            }
        }

        if (windowWidth <= 576) {
            $sharingBanner.css('right', 'auto');
        } else if ($sharingBanner.hasClass('fixed'))  {
            if (windowWidth <= 1920) {
                $sharingBanner.css('right', 0);
                $nav.css('right', 0);
            } else {
                var $mainpage = $('.page');
                var mainpageRight = windowWidth - parseInt($mainpage.offset().left) - parseInt($mainpage.outerWidth());
                $nav.css('right', mainpageRight);
                $sharingBanner.css('right', mainpageRight);
            }
        } else {
            $sharingBanner.css('right', 0);
        }

        var $menuButtonContainer = $('.aside').first();

        $('.mainpage__wrapper')
            .each(function() {
                var clientTop = parseInt($(this).offset().top) - parseInt(scrollTop);
                var clientBottom = clientTop + parseInt($(this).outerHeight());
                if (clientTop <= 0 && clientBottom >= 0) {
                    var addMenuClass = $(this).data('add-menu-class');
                    var removeMenuClass = $(this).data('remove-menu-class');
                    if (addMenuClass) {
                        $menuButtonContainer.addClass(addMenuClass);
                    }

                    if (removeMenuClass) {
                        $menuButtonContainer.removeClass(removeMenuClass);
                    }
                }
            })
        ;
    }

    $(document).ready(() => {
        checkMenus();

        $('.mainpage__slider').slick({
            arrows: false,
            cssEase: 'linear',
            dots: true,
            fade: true,
            infinite: false,
            adaptiveHeight: false,
            autoPlay: false
        });

        $('.page__block-popular-content-items').slick({
            adaptiveHeight: false,
            appendArrows: '.page__block-popular-content-arrows.down',
            arrows: true,
            dots: false,
            infinite: true,
            nextArrow:  '<button class="page__block-popular-content-arrow next">'
                    +   '  Следующий'
                    +   '</button>',
            prevArrow: '<button class="page__block-popular-content-arrow prev">'
                +   '  Предыдущий'
                +   '</button>',
            slidesToScroll: 1,
            slidesToShow: 5,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 576,
                    settings: {
                        appendArrows: '.page__block-popular-content-arrows.up',
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.page__block-news-content-last-news').slick({
            adaptiveHeight: false,
            // appendArrows: '.page__block-news-content-last-news-arrows.down',
            // arrows: true,
            arrows: false,
            dots: false,
            infinite: true,
            nextArrow:  '<button class="page__block-news-content-last-news-arrow next">'
                    +   '  Следующий'
                    +   '</button>',
            prevArrow: '<button class="page__block-news-content-last-news-arrow prev">'
                +   '  Предыдущий'
                +   '</button>',
            slidesToScroll: 1,
            slidesToShow: 3,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 576,
                    settings: {
                        // appendArrows: '.page__block-news-content-last-news-arrows.up',
                        slidesToShow: 1
                    }
                }
            ]
        });
    });

    $(document).on('click', '.page-block__nav-trigger', () => {
        $('body').toggleClass('nav-close');
    });

    $(document).on('click', ':not(.nav-close) .nav a', function() {
        $('body').addClass('nav-close');
    });

    $(document).on('click', '.page__block-playgrounds-description-show-all', function() {
        clearForm(document.getElementsByClassName('page__block-playgrounds-filter-form')[0]);
    });

    $(document).on('click', '.page__block-playgrounds-list-item-button-register.disabled', function() {
        var messageHtml = $('.page__block-playgrounds-register-form-disabled-container').html();

        var messageBox = window.SimpleLightbox.open({
            content: messageHtml,
            elementClass: 'slbContentEl'
        });

        $(document).on('click', '.page__block-playgrounds-register-form-disabled-close-button', function() {
            messageBox.close();
        });
    });

    $(document).on('click', '.page__block-playgrounds-list-item-button-register:not(.disabled)', function() {
        var $tourBlock = $(this).closest('.page__block-playgrounds-list-item');
        var tourName = $tourBlock.find('.page__block-playgrounds-list-item-header-title-name').text().trim();
        var tourDate = $tourBlock.find('.page__block-playgrounds-list-item-header-title-date').text().trim();

        // заменить на URL, где будут в таком же форматы выдаваться даты и варианты времени по конкретной экскурсии
        $.get('/moscow/guide-day/js/schedule.json', function(schedule) {
            var idsToReplaceWithRandoms = [];

            var enableDates = Object
                .keys(schedule)
                .map(function(mySqlDate) {
                    var dateParsed = dayjs(mySqlDate);
                    return dateParsed.format('D-M-YYYY');
                })
            ;

            var firstDates = Object
                .keys(schedule)
                .map(function(mySqlDate) {
                    var dateParsed = dayjs(mySqlDate);
                    return dateParsed.format('DD-MM-YYYY');
                })
                .slice(0, 3);

            $('.page__block-playgrounds-register-form-date-item-input').remove();
            $('.page__block-playgrounds-register-form-date-item-value').remove();
            $('.page__block-playgrounds-register-form-places-count').hide();
            $('.page__block-playgrounds-register-form-time-title').hide();
            $('.page__block-playgrounds-register-form-time').hide();

            var $dateContainer = $('.page__block-playgrounds-register-form-date');

            Object
                .keys(schedule)
                .slice(0, 3)
                .map(function(mySqlDate) {
                    return dayjs(mySqlDate);
                })
                .forEach(function(date, index) {
                    $dateContainer.append([
                        '<input',
                        '    id="page__block-playgrounds-register-form-date-item-input-' + index + '"',
                        '    type="radio"',
                        '    class="page__block-playgrounds-register-form-date-item-input"',
                        '    value="' + date.format('YYYY-MM-DD') + '"',
                        '    name="date"',
                        '/>',
                        '<label',
                        '    for="page__block-playgrounds-register-form-date-item-input-' + index + '"',
                        '    class="page__block-playgrounds-register-form-date-item-value"',
                        '>',
                        '    ', date.format('dd, D MMMM'),
                        '</label>'
                    ].join(''));
                    idsToReplaceWithRandoms.push('page__block-playgrounds-register-form-date-item-input-' + index);
                })
            ;

            $('.page__block-playgrounds-register-form-time-item-input').remove();
            $('.page__block-playgrounds-register-form-time-item-value').remove();

            function enableAllTheseDays(date) {
                var sdate = $.datepicker.formatDate( 'd-m-yy', date)
                // console.log(sdate);
                if($.inArray(sdate, enableDates) != -1) {
                    return [true];
                }
                return [false];
            }

            function processDate(date, $timeContainer) {
                $('.page__block-playgrounds-register-form-time-item-input').remove();
                $('.page__block-playgrounds-register-form-time-item-value').remove();
                var $form = $timeContainer.closest('form');
                $form.find('.page__block-playgrounds-register-form-places-count').hide();

                var showTime = false;
                var dateFormatted = date.format('YYYY-MM-DD');
                Object
                    .keys(schedule[dateFormatted])
                    .filter(
                        function(time) {
                            var placesCount = schedule[dateFormatted][time];
                            return !!placesCount;
                        }
                    )
                    .forEach(
                        function(time, index) {
                            showTime = true;
                            $timeContainer.append([
                                '<input',
                                '    id="page__block-playgrounds-register-form-time-item-input-' + index + '"',
                                '    type="radio"',
                                '    class="page__block-playgrounds-register-form-time-item-input"',
                                '    value="' + time + '"',
                                '    name="time"',
                                '/>',
                                '<label',
                                '    for="page__block-playgrounds-register-form-time-item-input-' + index + '"',
                                '    class="page__block-playgrounds-register-form-time-item-value"',
                                '>',
                                '    ', time,
                                '</label>'
                            ].join(''));
                            idsToReplaceWithRandoms.push('page__block-playgrounds-register-form-time-item-input-' + index);
                        }
                    );

                if (showTime) {
                    $form.find('.page__block-playgrounds-register-form-time-title').show();
                    $form.find('.page__block-playgrounds-register-form-time').show();
                }
            }

            $(document).on('click', '.page__block-playgrounds-register-form-date-item-value-custom', function() {
                var $button = $(this);
                var $buttonText = $button.find('.page__block-playgrounds-register-form-date-item-value-custom-text');
                var $datePickerInput = $button.find('.page__block-playgrounds-register-form-date-item-input-custom');
                var defaultDate = null;
                if ($datePickerInput.val()) {
                    defaultDate = dayjs($datePickerInput.val());
                    $datePickerInput.datepicker('option', 'defaultDate', defaultDate.toDate());
                }
                $datePickerInput.datepicker({
                    dateFormat: 'dd-mm-yy',
                    defaultDate: defaultDate ? defaultDate.toDate() : null,
                    beforeShowDay: enableAllTheseDays,
                    onSelect: function(selectedDate, $datePicker) {
                        var dateParsed = dayjs(selectedDate, 'DD-MM-YYYY');
                        var dateParsedMySql = dateParsed.format('YYYY-MM-DD');
                        $('.page__block-playgrounds-register-form-date-item-input').prop('checked', false);
                        if (firstDates.indexOf(selectedDate) !== -1) {
                            $('.page__block-playgrounds-register-form-date-item-input[value="' + dateParsedMySql + '"]')
                                .prop('checked', true)
                            ;
                            $button.removeClass('checked');
                        } else {
                            $datePickerInput
                                .val(dateParsedMySql)
                                .prop('checked', true)
                            ;
                            $button.addClass('checked');
                            $buttonText.text(dateParsed.format('dd, D MMMM'));
                        }
                        // $datePickerInput.datepicker('hide');
                        processDate(dateParsed, $button.closest('form').find('.page__block-playgrounds-register-form-time'));
                    }
                });
                $datePickerInput.datepicker('show');

                $(document).on('click', '.page__block-playgrounds-register-form-date-item-value', function() {
                    $button.removeClass('checked');
                    $buttonText.text('Другая дата');
                    $datePickerInput.prop('checked', false);
                });
            });

            $(document).on('click', '.page__block-playgrounds-register-form-date-item-value', function() {
                var $input = $(this).prev();
                var dateParsed = dayjs($input.val());
                processDate(dateParsed, $(this).closest('form').find('.page__block-playgrounds-register-form-time'));
            });

            $(document).on('click', '.page__block-playgrounds-register-form-time-item-value', function() {
                var $form = $(this).closest('form');
                var dateChosen = $form.find('input[name="date"]:checked').val();
                var timeChosen = $(this).prev().val();
                var dateParsed = dayjs(dateChosen);
                var $placesCount = $form.find('.page__block-playgrounds-register-form-places-count');
                $placesCount.text([
                    dateParsed.format('dddd, D MMMM'),
                    ', ',
                    timeChosen,
                    ', свободно мест: ',
                    schedule[dateChosen][timeChosen]
                ].join(''));
                $placesCount.show();
            });

            var formContainerSelectors = [
                // '.page__block-playgrounds-register-form-container',  // закомментируйте эту строку, чтобы скрыть форму1
                '.page__block-playgrounds-register-form-2-container',// закомментируйте эту строку, чтобы скрыть форму2
            ];
            var formContainerSelectorIndex = getRandomInt(formContainerSelectors.length - 1);
            var formContainerSelector = formContainerSelectors[formContainerSelectorIndex];

            var registerFormRawHtml = $(formContainerSelector).html();
            idsToReplaceWithRandoms.forEach(function(id) {
                registerFormRawHtml = registerFormRawHtml.replaceAll(id, makeid(15));
            });
            var registerFormHtml = registerFormRawHtml
                .replace('${tourName}', tourName)
                .replace('${tourDate}', tourDate)
            ;

            // чтобы протестировать форму, раскомментируйте код ниже
            // $(document).on('click', '.page__block-playgrounds-register-form-submit-button', function() {
            //     console.log($(this).closest('form').serialize());
            //     return false;
            // });

            window.SimpleLightbox.open({
                content: registerFormHtml,
                elementClass: 'slbContentEl'
            });
        });
    });

    $(document).on('click', '.toggle-playground', function() {
        var $playground = $(this).closest('.page__block-playgrounds-list-item');
        $playground.toggleClass('expanded');
        var $photoGalleryContainer = $playground.find('.page__block-playgrounds-list-item-photos');
        if ($photoGalleryContainer.length && !$photoGalleryContainer.hasClass('slick-initialized')) {
            $photoGalleryContainer.slick({
                adaptiveHeight: false,
                appendArrows: $playground.find('.page__block-playgrounds-list-item-photos-arrow-buttons.up'),
                arrows: true,
                dots: false,
                infinite: true,
                nextArrow:  '<button class="page__block-playgrounds-list-item-photos-arrow next">'
                    +   '  Следующий'
                    +   '</button>',
                prevArrow: '<button class="page__block-playgrounds-list-item-photos-arrow prev">'
                    +   '  Предыдущий'
                    +   '</button>',
                slidesToScroll: 1,
                slidesToShow: 3,
                variableWidth: false,
                responsive: [
                    {
                        breakpoint: 576,
                        settings: {
                            appendArrows: $playground.find(
                                '.page__block-playgrounds-list-item-photos-arrow-buttons.down'
                            ),
                            slidesToShow: 1
                        }
                    }
                ]
            });
        } else {
            // console.log('something is wrong');
        }
    });


    $(document).on('click', '.page__block-playgrounds-list-item-photo-container', function(e) {
        var $currentPhotoContainers = $(this)
            .closest('.page__block-playgrounds-list-item-photos')
            .find('.page__block-playgrounds-list-item-photo-container:not(.slick-cloned)')
        ;
        var currentUrl = $(this).attr('href');
        var currentIndex = 0;
        $currentPhotoContainers.each(function(index, element) {
            if ($(this).attr('href') === currentUrl) {
                currentIndex = index;
                return false;
            }
        });

        window.SimpleLightbox.open({
            items: $currentPhotoContainers.map(function() {
                return $(this).attr('href');
            }),
            captions: $currentPhotoContainers.map(function() {
                var $img = $(this).find('.page__block-playgrounds-list-item-photo-img');
                return $img.attr('title') || $img.attr('alt') || '';
            }),
            startAt: currentIndex,
            loadingCaption: 'Загрузка...'
        });
        return false;

    });

    $(window).on('scroll', checkMenus);

    $(window).on('resize', checkMenus);
})(jQuery);