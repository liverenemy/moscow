/* Russian initialisation for the jQuery UI date picker plugin. */
( function( factory ) {
    if ( typeof define === "function" && define.amd ) {

        // AMD. Register as an anonymous module.
        define( [ "../widgets/datepicker" ], factory );
    } else {

        // Browser globals
        factory( jQuery.datepicker );
    }
}( function( datepicker ) {

    datepicker.regional.ru = {
        closeText: "Закрыть",
        prevText: "пред.",
        nextText: "след.",
        currentText: "Сегодня",
        monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
        monthNamesShort: [ "янв.", "фев.", "март", "апр.", "май", "июнь",
            "июль", "авг.", "сент.", "окт.", "ноя.", "дек." ],
        dayNames: [ "воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота" ],
        dayNamesShort: [ "вс", "пн", "вт", "ср", "чт", "пт", "сб" ],
        dayNamesMin: [ "в","п","в","с","ч","п","с" ],
        weekHeader: "Нед.",
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: "" };
    datepicker.setDefaults( datepicker.regional.fr );

    return datepicker.regional.fr;

} ) );