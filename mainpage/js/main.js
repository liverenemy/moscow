(function($) {
    $.fn.reverse = [].reverse;

    function checkMenus() {
        var $window = $(window);
        var windowWidth = parseInt($window.outerWidth());

        var scrollTop = $window.scrollTop();


        var $sharingBanner = $('.sharing');
        var sharingMinimumScrollTop = 1080;
        var isDesktopLayout = windowWidth > 800;

        if (windowWidth <= 1920) {
            sharingMinimumScrollTop = sharingMinimumScrollTop / 1920 * windowWidth;
        }

        if (isDesktopLayout && scrollTop > sharingMinimumScrollTop) {
            if (!$sharingBanner.hasClass('fixed')) {
                $sharingBanner.addClass('fixed');
            }
        } else {
            if ($sharingBanner.hasClass('fixed')) {
                $sharingBanner.removeClass('fixed');
            }
        }

        if (windowWidth <= 800) {
            $sharingBanner.css('right', 'auto');
        } else if ($sharingBanner.hasClass('fixed'))  {
            if (windowWidth <= 1920) {
                $sharingBanner.css('right', 0);
            } else {
                var $mainpage = $('.mainpage');
                var mainpageRight = windowWidth - parseInt($mainpage.offset().left) - parseInt($mainpage.outerWidth());
                $sharingBanner.css('right', mainpageRight);
            }
        } else {
            $sharingBanner.css('right', 0);
        }

        var $menuButtonContainer = $('.aside').first();

        $('.mainpage__wrapper')
            .each(function() {
                var clientTop = parseInt($(this).offset().top) - parseInt(scrollTop);
                var clientBottom = clientTop + parseInt($(this).outerHeight());
                if (clientTop <= 0 && clientBottom >= 0) {
                    var addMenuClass = $(this).data('add-menu-class');
                    var removeMenuClass = $(this).data('remove-menu-class');
                    if (addMenuClass) {
                        $menuButtonContainer.addClass(addMenuClass);
                    }

                    if (removeMenuClass) {
                        $menuButtonContainer.removeClass(removeMenuClass);
                    }
                }
            })
        ;
    }

    $(document).ready(() => {
        checkMenus();

        $('.mainpage__slider').slick({
            arrows: false,
            cssEase: 'linear',
            dots: true,
            fade: true,
            infinite: false,
            adaptiveHeight: false,
            autoPlay: false
        });

        $('.mainpage-popular__gallery').slick({
            adaptiveHeight: false,
            appendArrows: '.mainpage-popular__gallery-arrows',
            arrows: true,
            dots: false,
            infinite: true,
            nextArrow:  '<button class="mainpage-popular__gallery-next-button">'
                    +   '  Следующий'
                    +   '<img src="./img/svg/popular/gallery-next-arrow.svg" />'
                    +   '</button>',
            prevArrow: '<button class="mainpage-popular__gallery-prev-button">'
                +   '<img src="./img/svg/popular/gallery-prev-arrow.svg" />'
                +   '  Предыдущий'
                +   '</button>',
            slidesToScroll: 1,
            slidesToShow: 3,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 1165,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 605,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    });

    $(document).on('click', '.mainpage__nav-trigger', () => {
        $('body').toggleClass('nav-close');
    });

    $(document).on('click', '.page-block__nav-trigger', () => {
        $('body').toggleClass('nav-close');
    });

    $(document).on('click', '.overlay__feedback-close-button', function() {
        $('body')
            .removeClass('show-feedback')
            .removeClass('show-overlay')
        ;
    });

    $(document).on('click', '.mainpage__subscribe-form-submit-button', function(event) {
        event.preventDefault();

        $('body')
            .addClass('show-overlay')
            .addClass('show-thank-you')
        ;
    });

    $(document).on('click', '.mainpage__feedback-button', function() {
        $('body')
            .addClass('show-overlay')
            .addClass('show-feedback')
        ;
    });

    $(document).on('click', '.overlay__thank-you-close-button', function() {
        $('body')
            .removeClass('show-thank-you')
            .removeClass('show-overlay')
        ;
    });

    $(document).on('click', '.overlay__feedback-comment-submit-button', function() {
        $('body')
            .removeClass('show-feedback')
            .addClass('show-thank-you')
        ;
    });

    $(window).on('scroll', checkMenus);

    $(window).on('resize', checkMenus);
})(jQuery);